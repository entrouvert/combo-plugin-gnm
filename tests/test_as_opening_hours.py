import json
import os

import pytest
from django.utils.safestring import mark_safe

from combo_plugin_gnm.templatetags.gnm import EN_FULL_WEEKDAYS_LIST, as_opening_hours_badge

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
with open(os.path.join(BASE_DIR, 'tests/data/mairie-geojson.json')) as fd:
    GEOJSON = json.load(fd)['features']
with open(os.path.join(BASE_DIR, 'tests/data/mdr-geojson.json')) as fd:
    MDR_GEOJSON = json.load(fd)['features']
TZOFFSETS = {'Europe/Paris': 3600}


@pytest.mark.freeze_time(
    '2018-03-04 23:00:00',
)
def test_every_mairie_closed():
    """every mairie is closed at mignight"""
    opening_hours = [as_opening_hours_badge(x) for x in GEOJSON]
    assert len([x for x in opening_hours if 'open' in x]) == 0


@pytest.mark.freeze_time('2018-03-04 23:00:00')
def test_all_mairie_data_parsed_correct():
    """everything got parsed correctly"""
    opening_hours = [
        as_opening_hours_badge(x)
        for x in GEOJSON
        if x['properties'].get('openinghours') or x['properties'].get('openinghoursspecification')
    ]
    assert opening_hours.count('') == 0


@pytest.mark.freeze_time('2018-03-05 14:59:00')
def test_empty_data():
    """ "no data return the empty html"""
    test_html = as_opening_hours_badge(GEOJSON[0]['properties'])
    assert test_html == ''


@pytest.mark.freeze_time('2018-08-13 14:59:00')
def test_mairie_bron_monday():
    """S1326"""
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1326'][0]
    klass, label = 'open', "Ouvert jusqu'à 17h15"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-08-12 14:59:00')
def test_mairie_bron_sunday():
    """S1326"""
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1326'][0]
    klass, label = 'closed', 'Réouvre demain à 8h00'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-08-09 13:30:01')
def test_mairie_sathonay_thursday_afternoon():
    """S1326"""
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1415'][0]
    klass, label = 'open', "Ouvert jusqu'à 17h00"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-08-11 11:30:01')
def test_mairie_saint_priest_closed():
    """S1326"""
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1406'][0]
    klass, label = 'closed', 'Réouvre lundi à 8h15'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-08-22 12:13:01')
def test_mairie_saint_priest_open():
    """S1326"""
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1406'][0]
    klass, label = 'soon-to-be-closed', "Ouvert jusqu'à 12h15"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-08-11 14:59:00')
def test_mairie_bron_saturday():
    """S1326"""
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1326'][0]
    klass, label = 'closed', 'Réouvre lundi à 8h00'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-05 09:59:13')
def test_jonage_open():
    'Jonage is defined only by openinghoursspecification data'
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['nom'] == 'Mairie de Jonage'][
        0
    ]
    klass, label = 'open', "Ouvert jusqu'à 12h30"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-05 11:32:00')
def test_jonage_soon_to_be_closed():
    'Jonage is defined only by openinghoursspecification data'
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['nom'] == 'Mairie de Jonage'][
        0
    ]
    klass, label = 'soon-to-be-closed', "Ouvert jusqu'à 12h30"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-10 17:30:00')
def test_jonage_closed():
    'Jonage is defined only by openinghoursspecification data'
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['nom'] == 'Mairie de Jonage'][
        0
    ]
    klass, label = 'closed', 'Réouvre lundi à 8h30'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-05 15:55:00')
def test_data_input_compatibility():
    """as_opening_hours with a fixed datetime"""
    klass = 'open'
    label = "Ouvert jusqu'à 17h00"
    test_html = as_opening_hours_badge(GEOJSON[1]['properties'])
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))
    test_html = as_opening_hours_badge(GEOJSON[1])
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-05 00:00:00')
def test_all_mdr_closed():
    """every mdr is closed at mignight"""
    opening_hours = [as_opening_hours_badge(x) for x in MDR_GEOJSON]
    assert len([x for x in opening_hours if 'open' in x]) == 0


def test_all_mdr_data_parsed_correct():
    """everything got parsed correctly"""
    opening_hours = [as_opening_hours_badge(x) for x in MDR_GEOJSON]
    assert opening_hours.count('') == 0


@pytest.mark.freeze_time('2018-03-05 14:59:00')
def test_mdr_open():
    test_html = as_opening_hours_badge(MDR_GEOJSON[0])
    klass, label = 'open', "Ouvert jusqu'à 16h45"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-05 15:46:00')
def test_mdr_soon_to_be_closed():
    test_html = as_opening_hours_badge(MDR_GEOJSON[0])
    klass, label = 'soon-to-be-closed', "Ouvert jusqu'à 16h45"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-10 17:30:00')
def test_mdr_just_closed():
    test_html = as_opening_hours_badge(MDR_GEOJSON[0])
    klass, label = 'closed', 'Réouvre lundi à 8h30'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-01-01 14:59:00')
def test_mairie_holiday():
    # Ecully, using datetimes
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1361'][0]
    klass, label = 'closed', 'Réouvre demain à 8h30'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))

    # Feyzin, using dates
    test_html = [as_opening_hours_badge(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1365'][0]
    klass, label = 'closed', 'Réouvre demain à 8h30'
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-03-05 00:30:00')
@pytest.mark.parametrize(
    'day, hours, badge, text',
    [
        ('mercredi_pm', '14h30-17h45', 'closed', 'Réouvre mercredi à 14h30'),
        ('mercredi_pm', '00h00-24h00', 'closed', 'Réouvre mercredi 24h/24'),
        ('mardi_pm', '14h30-17h45', 'closed', 'Réouvre demain à 14h30'),
        ('mardi_pm', '14h30-00h15', 'closed', 'Réouvre demain à 14h30'),
        ('lundi_pm', '14h30-17h45', 'closed', 'Réouvre  à 14h30'),
        ('lundi_am', '08h30-11h45', 'closed', 'Ouvre  à 8h30'),
        ('lundi_am', '00h00-00h45', 'soon-to-be-closed', "Ouvert jusqu'à 0h45"),
        ('dimanche_pm', '20h30-00h45', 'soon-to-be-closed', "Ouvert jusqu'à 0h45"),
        ('lundi_am', '00h15-24h00', 'open', "Ouvert jusqu'à minuit"),
        ('lundi_am', '00h00-24h00', 'open', 'Ouvert 24h/24'),
    ],
)
def test_mdr_format(day, hours, badge, text):
    geojson = """
{
  "properties": {
    "%s" : "%s"
  }
}
""" % (
        day,
        hours,
    )
    html = as_opening_hours_badge(json.loads(geojson))
    assert html == '<div class="badge %s"><span>%s</span></div>' % (badge, text)


@pytest.mark.freeze_time('2018-03-05 00:30:00')
@pytest.mark.parametrize(
    'openinghour, badge, text',
    [
        ('We 14:30-17:45', 'closed', 'Réouvre mercredi à 14h30'),
        ('We 00:00-24:00', 'closed', 'Réouvre mercredi 24h/24'),
        ('Tu 14:30-17:45', 'closed', 'Réouvre demain à 14h30'),
        ('Tu 14:30-00:15', 'closed', 'Réouvre demain à 14h30'),
        ('Mo 14:30-17:45', 'closed', 'Réouvre  à 14h30'),
        ('Mo 08:30-11:45', 'closed', 'Ouvre  à 8h30'),
        ('Mo 00:00-00:45', 'soon-to-be-closed', "Ouvert jusqu'à 0h45"),
        ('Su 20:30-00:45', 'soon-to-be-closed', "Ouvert jusqu'à 0h45"),
        ('Mo 00:15-24:00', 'open', "Ouvert jusqu'à minuit"),
        ('Mo 00:00-24:00', 'open', 'Ouvert 24h/24'),
    ],
)
def test_mairie_format_openinghours(openinghour, badge, text):
    geojson = (
        """
{
  "properties": {
      "openinghours": ["%s"]
  }
}
"""
        % openinghour
    )
    html = as_opening_hours_badge(json.loads(geojson))
    assert html == '<div class="badge %s"><span>%s</span></div>' % (badge, text)


@pytest.mark.freeze_time('2018-03-05 00:30:00')
@pytest.mark.parametrize(
    'day, opens, closes, badge, text',
    [
        ('Wednesday', '14:30', '17:45', 'closed', 'Réouvre mercredi à 14h30'),
        ('Wednesday', '00:00', '24:00', 'closed', 'Réouvre mercredi 24h/24'),
        ('Tuesday', '14:30', '17:45', 'closed', 'Réouvre demain à 14h30'),
        ('Tuesday', '14:30', '00:15', 'closed', 'Réouvre demain à 14h30'),
        ('Monday', '14:30', '17:45', 'closed', 'Réouvre  à 14h30'),
        ('Monday', '08:30', '11:45', 'closed', 'Ouvre  à 8h30'),
        ('Monday', '00:00', '00:45', 'soon-to-be-closed', "Ouvert jusqu'à 0h45"),
        ('Sunday', '20:30', '00:45', 'soon-to-be-closed', "Ouvert jusqu'à 0h45"),
        ('Monday', '00:15', '24:00', 'open', "Ouvert jusqu'à minuit"),
        ('Monday', '00:00', '24:00', 'open', 'Ouvert 24h/24'),
    ],
)
def test_mairie_format_openinghoursspecification(day, opens, closes, badge, text):
    geojson = r"""
{
  "properties": {
      "openinghoursspecification": [{
        "opens": "%s",
        "closes": "%s",
        "dayOfWeek": "http:\/\/schema.org\/%s",
        "validFrom": "2018-01-01T00:00:00+01:00",
        "validThrough": "2018-06-30T23:59:59+01:00"
      }]
  }
}
""" % (
        opens,
        closes,
        day,
    )
    html = as_opening_hours_badge(json.loads(geojson))
    assert html == '<div class="badge %s"><span>%s</span></div>' % (badge, text)


@pytest.mark.freeze_time('2018-03-09 00:30:00')
def test_mairie_having_both_formats():
    """openinghoursspecification take preference over openinghours"""
    geojson = r"""
{
  "properties": {
    "openinghours": [
      "Mo 08:30-11:30"
    ],
    "openinghoursspecification": [{
      "opens": "09:30",
      "closes": "12:30",
      "dayOfWeek": "http:\/\/schema.org\/Monday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }]
  }
}
"""
    html = as_opening_hours_badge(json.loads(geojson))
    assert html == '<div class="badge %s"><span>%s</span></div>' % ('closed', 'Réouvre lundi à 9h30')


@pytest.mark.freeze_time('2021-01-21 15:37:00')
def test_mairie_saint_genis_lavak():
    """#50337"""
    with open(os.path.join(BASE_DIR, 'tests/data/mairie-saint-genis-lavak.json')) as fd:
        geojson = json.load(fd)
    test_html = as_opening_hours_badge(geojson['features'][0])
    klass, label = 'open', "Ouvert jusqu'à 17h30"
    assert test_html == mark_safe('<div class="badge %s"><span>%s</span></div>' % (klass, label))


@pytest.mark.freeze_time('2018-01-04 14:59:00')
@pytest.mark.parametrize(
    'validFrom, validTrought, badge, message',
    [
        ('2017-12-01', '2018-02-01', 'closed', 'Fermé'),
        ('2018-01-04', '2018-01-04', 'closed', 'Réouvre demain à 8h30'),
        ('2018-01-04', '2018-01-09', 'closed', 'Réouvre mercredi à 8h30'),
        ('2018-01-04', '2018-01-11', 'closed', 'Fermé'),
        ('2018-01-05', '2018-01-11', 'open', "Ouvert jusqu'à 17h30"),
    ],
)
def test_mairie_holiday_period(validFrom, validTrought, badge, message):
    ohs = []
    for weekday in EN_FULL_WEEKDAYS_LIST:
        ohs.append(
            {
                'opens': '08:30',
                'closes': '17:30',
                'dayOfWeek': 'http://schema.org/%s' % weekday,
                'validFrom': '2018-01-01T00:00:00+01:00',
                'validThrough': '2018-06-30T23:59:59+01:00',
            }
        )
    ohs.append(
        {
            'validFrom': '%sT00:00:00+01:00' % validFrom,
            'validThrough': '%sT23:59:59+01:00' % validTrought,
        }
    )
    data = {'properties': {'openinghoursspecification': ohs}}
    html = as_opening_hours_badge(data)
    assert html == '<div class="badge %s"><span>%s</span></div>' % (badge, message)
