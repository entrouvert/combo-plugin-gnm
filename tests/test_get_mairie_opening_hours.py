import json
import os

import pytest

from combo_plugin_gnm.templatetags.gnm import EN_FULL_WEEKDAYS_LIST, FR_WEEKDAYS, get_mairie_opening_hours

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
with open(os.path.join(BASE_DIR, 'tests/data/mairie-geojson.json')) as fd:
    GEOJSON = json.load(fd)['features']

TZOFFSETS = {'Europe/Paris': 3600}


def test_mairie_hours_parsing():
    """everything is parsed correctly"""
    opening_hours = [get_mairie_opening_hours(x) for x in GEOJSON]
    assert len(opening_hours) == len(GEOJSON)


def test_mairie_hours_nodata():
    """ "no data return nothing"""
    assert get_mairie_opening_hours(GEOJSON[0]['properties']) is None


def test_mairie_hours_special_data():
    """test results for various data examples"""
    for x in GEOJSON:
        if x['properties']['identifiant'] == 'S1376':
            # La Mulatière : openinghoursspecification set but without opens/closes
            assert get_mairie_opening_hours(x) == [
                ('lundi', {'am': '08h30-12h00', 'pm': None}),
                ('mardi', {'am': '08h30-12h30', 'pm': '13h30-17h00'}),
                ('mercredi', {'am': '08h30-12h30', 'pm': '13h30-17h00'}),
                ('jeudi', {'am': '08h30-12h30', 'pm': '13h30-17h00'}),
                ('vendredi', {'am': '08h30-12h30', 'pm': '13h30-17h00'}),
                ('samedi', {'am': '09h00-11h45', 'pm': None}),
            ]

        if x['properties']['identifiant'] == 'S1437':
            x['properties']['openinghoursspecification'] = []  # force using openinghours
            # special openinghours format with days intervals, comma-separated list and one day definition with a saturday
            assert get_mairie_opening_hours(x) == [
                ('lundi', {'am': '08h45-12h30', 'pm': '14h00-16h45'}),
                ('mardi', {'am': '08h45-16h45', 'pm': ''}),
                ('mercredi', {'am': '08h45-16h45', 'pm': ''}),
                ('jeudi', {'am': '08h45-18h00', 'pm': ''}),
                ('vendredi', {'am': '08h45-16h45', 'pm': ''}),
                ('samedi', {'am': '09h00-12h00', 'pm': None}),
            ]

        if x['properties']['identifiant'] == 'S5564':
            x['properties']['openinghoursspecification'] = []  # force using openinghours
            # classic openinghours days interval for am and pm
            assert get_mairie_opening_hours(x) == [
                ('lundi', {'am': '08h30-12h15', 'pm': '13h15-17h00'}),
                ('mardi', {'am': '08h30-12h15', 'pm': '13h15-17h00'}),
                ('mercredi', {'am': '08h30-12h15', 'pm': '13h15-17h00'}),
                ('jeudi', {'am': '08h30-12h15', 'pm': '13h15-17h00'}),
                ('vendredi', {'am': '08h30-12h15', 'pm': '13h15-17h00'}),
            ]


@pytest.mark.freeze_time('2018-03-05 15:59:00')
def test_mairie_openinghoursspecification_period_valid():
    """Test valid periods of openinghoursspecification timetables"""
    for x in GEOJSON:
        if x['properties']['nom'] == 'Mairie de Jonage':
            assert get_mairie_opening_hours(x) == [
                ('lundi', {'am': '08h30-12h30', 'pm': '14h00-17h00'}),
                ('mardi', {'am': '08h30-12h30', 'pm': '14h00-17h00'}),
                ('mercredi', {'am': '08h30-12h30', 'pm': '14h00-17h00'}),
                ('jeudi', {'am': '08h30-12h30', 'pm': '14h00-17h00'}),
                ('vendredi', {'am': '08h30-12h30', 'pm': '14h00-17h00'}),
                ('samedi', {'am': '09h00-12h00', 'pm': '14h00-17h00'}),
            ]
            return


@pytest.mark.freeze_time('2020-03-05 15:59:00')
def test_mairie_openinghoursspecification_period_all_closed():
    # display known format but no opening hours as all closed
    for x in GEOJSON:
        if x['properties']['nom'] == 'Mairie de Jonage':
            assert get_mairie_opening_hours(x) == [
                ('lundi', {'am': None, 'pm': ''}),
                ('mardi', {'am': None, 'pm': ''}),
                ('mercredi', {'am': None, 'pm': ''}),
                ('jeudi', {'am': None, 'pm': ''}),
                ('vendredi', {'am': None, 'pm': ''}),
                ('samedi', {'am': None, 'pm': ''}),
                ('dimanche', {'am': None, 'pm': ''}),
            ]
            return


def test_mairie_sathonay_timetable():
    """Sathonay-Village S1415"""
    test_time_table = [
        get_mairie_opening_hours(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1415'
    ][0]
    assert test_time_table == [
        ('lundi', {'am': '08h30-12h00', 'pm': '14h00-17h00'}),
        ('mardi', {'am': '08h30-12h00', 'pm': '13h30-17h00'}),
        ('mercredi', {'am': '08h30-12h00', 'pm': '14h00-17h00'}),
        ('jeudi', {'am': '08h30-12h00', 'pm': '14h00-17h00'}),
        ('vendredi', {'am': '08h30-12h00', 'pm': '14h00-17h00'}),
        ('samedi', {'am': '08h30-12h00', 'pm': None}),
    ]


def test_mairie_saint_priest():
    'S1406'
    test_time_table = [
        get_mairie_opening_hours(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1406'
    ][0]
    assert test_time_table == [
        ('lundi', {'am': '08h15-12h15', 'pm': '13h30-17h30'}),
        ('mardi', {'am': '08h15-12h15', 'pm': '13h30-17h30'}),
        ('mercredi', {'am': '08h15-12h15', 'pm': '13h30-17h30'}),
        ('jeudi', {'am': '08h15-11h15', 'pm': '13h30-17h30'}),
        ('vendredi', {'am': '08h15-12h15', 'pm': '13h30-17h30'}),
        ('samedi', {'am': '09h00-11h30', 'pm': None}),
    ]


def test_mairie_format_openinghours():
    """some mairie may still only define the openinghours format"""
    geojson = """
{
  "properties": {
    "openinghours": [
      "Mo 08:30-11:30",
      "Tu 14:00-17:00",
      "We 10:30-18:30",
      "Th 22:30-00:30",
      "Fr 07:30-07:00",
      "Sa 21:00-24:00",
      "Su 00:00-24:00"
]}}
"""
    hours = get_mairie_opening_hours(json.loads(geojson))
    assert hours == [
        ('lundi', {'am': '08h30-11h30', 'pm': None}),
        ('mardi', {'am': None, 'pm': '14h00-17h00'}),
        ('mercredi', {'am': '10h30-18h30', 'pm': ''}),
        ('jeudi', {'am': None, 'pm': '22h30-00h30'}),
        ('vendredi', {'am': '07h30-07h00', 'pm': ''}),
        ('samedi', {'am': None, 'pm': '21h00-24h00'}),
        ('dimanche', {'am': '00h00-24h00', 'pm': ''}),
    ]


@pytest.mark.freeze_time('2018-03-09 00:30:00')
def test_mairie_format_openinghoursspecification():
    """openinghoursspecification the default format"""
    geojson = r"""
{
  "properties": {
    "openinghoursspecification": [{
      "opens": "08:30",
      "closes": "11:30",
      "dayOfWeek": "http:\/\/schema.org\/Monday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }, {
      "opens": "14:00",
      "closes": "17:00",
      "dayOfWeek": "http:\/\/schema.org\/Tuesday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }, {
      "opens": "10:30",
      "closes": "18:30",
      "dayOfWeek": "http:\/\/schema.org\/Wednesday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }, {
      "opens": "22:30",
      "closes": "00:30",
      "dayOfWeek": "http:\/\/schema.org\/Thursday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }, {
      "opens": "07:30",
      "closes": "07:00",
      "dayOfWeek": "http:\/\/schema.org\/Friday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }, {
      "opens": "21:00",
      "closes": "24:00",
      "dayOfWeek": "http:\/\/schema.org\/Saturday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }, {
      "opens": "00:00",
      "closes": "24:00",
      "dayOfWeek": "http:\/\/schema.org\/Sunday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
}]}}
"""
    hours = get_mairie_opening_hours(json.loads(geojson))
    assert hours == [
        ('lundi', {'am': '08h30-11h30', 'pm': None}),
        ('mardi', {'am': None, 'pm': '14h00-17h00'}),
        ('mercredi', {'am': '10h30-18h30', 'pm': ''}),
        ('jeudi', {'am': None, 'pm': '22h30-00h30'}),
        ('vendredi', {'am': '07h30-07h00', 'pm': ''}),
        ('samedi', {'am': None, 'pm': '21h00-24h00'}),
        ('dimanche', {'am': '00h00-24h00', 'pm': ''}),
    ]


@pytest.mark.freeze_time('2018-03-09 00:30:00')
def test_mairie_having_both_formats():
    """openinghoursspecification take preference over openinghours"""
    geojson = r"""
{
  "properties": {
    "openinghours": [
      "Mo 08:30-11:30"
    ],
    "openinghoursspecification": [{
      "opens": "09:30",
      "closes": "12:30",
      "dayOfWeek": "http:\/\/schema.org\/Monday",
      "validFrom": "2018-01-01T00:00:00+01:00",
      "validThrough": "2018-06-30T23:59:59+02:00"
    }]
  }
}
"""
    hours = get_mairie_opening_hours(json.loads(geojson))
    assert hours[0] == ('lundi', {'am': '09h30-12h30', 'pm': None})


@pytest.mark.freeze_time('2021-01-21 15:37:00')
def test_mairie_saint_genis_lavak():
    """#50337"""
    with open(os.path.join(BASE_DIR, 'tests/data/mairie-saint-genis-lavak.json')) as fd:
        geojson = json.load(fd)
    test_time_table = get_mairie_opening_hours(geojson['features'][0])
    assert test_time_table == [
        ('lundi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('mardi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('mercredi', {'am': '08h30-12h00', 'pm': None}),
        ('jeudi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('vendredi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('samedi', {'am': '09h00-12h00', 'pm': None}),
    ]


@pytest.mark.freeze_time('2018-01-01 14:59:00')
def test_mairie_holiday_day():
    # Ecully, using datetimes
    test_time_table = [
        get_mairie_opening_hours(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1361'
    ][0]
    assert test_time_table == [
        ('lundi', {'am': None, 'pm': None}),
        ('mardi', {'am': '08h30-12h00', 'pm': '13h30-17h00'}),
        ('mercredi', {'am': '08h30-12h00', 'pm': '13h30-17h00'}),
        ('jeudi', {'am': '08h30-12h00', 'pm': '13h30-17h00'}),
        ('vendredi', {'am': '08h30-12h00', 'pm': '13h30-17h00'}),
        ('samedi', {'am': '08h30-12h00', 'pm': None}),
    ]

    # Feyzin, using dates
    test_time_table = [
        get_mairie_opening_hours(x) for x in GEOJSON if x['properties']['identifiant'] == 'S1365'
    ][0]
    assert test_time_table == [
        ('lundi', {'am': None, 'pm': None}),
        ('mardi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('mercredi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('jeudi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
        ('vendredi', {'am': '08h30-12h00', 'pm': '13h30-17h30'}),
    ]


@pytest.mark.freeze_time('2018-01-04 14:59:00')
@pytest.mark.parametrize(
    'validFrom, validTrought, is_open',
    [
        ('2017-12-01', '2018-02-01', [False, False, False, False, False, False, False]),
        ('2018-01-04', '2018-01-04', [True, True, True, False, True, True, True]),
        ('2018-01-05', '2018-01-07', [True, True, True, True, False, False, False]),
        ('2018-01-05', '2018-01-09', [False, False, True, True, False, False, False]),
        ('2018-01-05', '2018-01-11', [False, False, False, True, False, False, False]),
    ],
)
def test_mairie_holiday_period(validFrom, validTrought, is_open):
    # buid expected time table
    open_day = {'am': '08h30-17h30', 'pm': ''}
    closed_day = {'am': None, 'pm': None}
    expected = []
    if not any(is_open):
        expected = [(weekday, {'am': None, 'pm': ''}) for weekday in FR_WEEKDAYS]
    else:
        for index, weekday in enumerate(FR_WEEKDAYS):
            if is_open[index]:
                expected.append((weekday, open_day))
            elif weekday not in ['samedi', 'dimanche']:
                expected.append((weekday, closed_day))

    ohs = []
    for weekday in EN_FULL_WEEKDAYS_LIST:
        ohs.append(
            {
                'opens': '08:30',
                'closes': '17:30',
                'dayOfWeek': 'http://schema.org/%s' % weekday,
                'validFrom': '2018-01-01T00:00:00+01:00',
                'validThrough': '2018-06-30T23:59:59+01:00',
            }
        )
    ohs.append(
        {
            'validFrom': '%sT00:00:00+01:00' % validFrom,
            'validThrough': '%sT23:59:59+01:00' % validTrought,
        }
    )
    data = {'properties': {'openinghoursspecification': ohs}}
    time_table = get_mairie_opening_hours(data)
    assert time_table == expected
