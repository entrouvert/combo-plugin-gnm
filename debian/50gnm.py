if not 'combo_plugin_gnm' in INSTALLED_APPS:
    INSTALLED_APPS += ('combo_plugin_gnm',)
    TENANT_APPS += ('combo_plugin_gnm',)

import memcache

memcache.SERVER_MAX_VALUE_LENGTH = 10 * 1024 * 1024
