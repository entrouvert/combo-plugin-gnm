# combo-plugin-gnm - Combo GNM plugin
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    def handle(self, *args, **options):
        # update settings.json of GNM tenants

        self.extra_settings_filepath = os.path.join(connection.tenant.get_directory(), 'gnm-settings.json')
        if not os.path.exists(self.extra_settings_filepath):
            return

        settings_filepath = os.path.join(connection.tenant.get_directory(), 'settings.json')
        if os.path.exists(settings_filepath):
            with open(settings_filepath) as fd:
                self.settings_json = json.load(fd)
        else:
            self.settings_json = {}

        self.update_settings_dict()
        self.import_extra_settings()
        self.set_no_logs()

        self.settings_json['JSON_CELL_TYPES.update'] = self.settings_json['JSON_CELL_TYPES']
        del self.settings_json['JSON_CELL_TYPES']
        self.settings_json['COMBO_ASSET_SLOTS.update'] = self.settings_json['COMBO_ASSET_SLOTS']
        del self.settings_json['COMBO_ASSET_SLOTS']

        with open(settings_filepath + '.new', 'w') as fd:
            json.dump(self.settings_json, fd, indent=2, ensure_ascii=False, sort_keys=True)
        os.replace(settings_filepath + '.new', settings_filepath)

    def update_settings_dict(self):
        # noqa pylint: disable=possibly-unused-variable
        MELLON_ADD_AUTHNREQUEST_NEXT_URL_EXTENSION = True

        # noqa pylint: disable=possibly-unused-variable
        GNM_REPORTING = True

        # noqa pylint: disable=possibly-unused-variable
        COMBO_MAP_TILE_URLTEMPLATE = 'https://tiles.entrouvert.org/hdm/{z}/{x}/{y}.png'
        # noqa pylint: disable=possibly-unused-variable
        COMBO_MAP_ATTRIBUTION = "&copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>"
        # noqa pylint: disable=possibly-unused-variable
        COMBO_GEOCODING_SERVICE = 'https://nominatim.entrouvert.org'

        atmo_api_token = getattr(settings, 'ATMO_API_TOKEN', '')

        JSON_CELL_TYPES = self.settings_json.get('JSON_CELL_TYPES.update') or {}
        JSON_CELL_TYPES = {
            'velov': {
                'url': 'https://%(data_username)s:%(data_password)s@download.data.grandlyon.com/ws/rdata/jcd_jcdecaux.jcdvelov/all.json?field=gid&value=[gid]'
                % settings.DATA_CREDENTIALS,
                'name': 'Velov',
                'cache_duration': 60,
                'force_async': False,
                'auto_refresh': 90,
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'GID de la station'}],
            },
            'taxi': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/taxi/?property:gid={{gid}}',
                'name': 'Station de taxi',
                'cache_duration': 86400,
                'force_async': False,
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'GID de la station'}],
            },
            'mdr': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/mdr/?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Maison de la Métropole de Lyon',
                'toodego:page': True,
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'piscine': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/piscine/?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Piscine de la Métropole de Lyon',
                'toodego:page': True,
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'mairie': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/mairie/?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Mairie',
                'toodego:page': True,
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'decheterie': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/decheterie/?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Déchèterie',
                'toodego:page': True,
                'toodego:page-title-template': 'Déchèterie {{properties.nom}}',
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'donnerie': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/donnerie/?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Donnerie',
                'toodego:page': True,
                'toodego:page-title-template': 'Donnerie {{properties.nom}}',
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'bibliotheque': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/bibliotheque/?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Bibliothèque',
                'toodego:page': True,
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'aire-de-covoiturage': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/aire-de-covoiturage/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Aire de covoiturage',
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'Identifiant'}],
            },
            'cinema': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/cinema/?property:gid={{gid}}',
                'name': 'Cinéma',
                'cache_duration': 86400,
                'force_async': False,
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'GID du cinéma'}],
            },
            'musee': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/musee/?property:gid={{gid}}',
                'name': 'Musée',
                'cache_duration': 86400,
                'force_async': False,
                'toodego:page': False,
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'GID du musée'}],
            },
            'parc-et-jardin-ponctuel': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/parc-et-jardin-ponctuel/?property:gid={{gid}}',
                'name': 'Parc ou jardin',
                'cache_duration': 86400,
                'force_async': False,
                'toodego:page': False,
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'GID du parc ou jardin'}],
            },
            'airquality': {
                'url': '{% if q_lat or lat %}https://api.atmo-aura.fr/partenaires/v2/indice_request'
                '?longitude={% firstof q_lon lon %}&latitude={% firstof q_lat lat %}&'
                + atmo_api_token
                + '{% endif %}',
                'cache_duration': 300,
                'force_async': False,
                'name': "Qualité de l'air",
                'toodego:tile': 'air',
                'form': [
                    {
                        'varname': 'lat',
                        'type': 'string',
                        'label': 'Latitude',
                        'required': False,
                    },
                    {
                        'varname': 'lon',
                        'type': 'string',
                        'label': 'Longitude',
                        'required': False,
                    },
                ],
                'additional-data': [
                    {
                        'key': 'air2go',
                        'url': '{% if q_lat or lat %}'
                        'https://api.atmo-aura.fr/partenaires/v2/indice_request?'
                        'longitude={% firstof q_lon lon %}&latitude={% firstof q_lat lat %}'
                        '{% else %}'
                        "{% now \"Y-m-d\" as today %}"
                        "{% with today|add_days:\"-1\"|date:\"Y-m-d\" as yesterday %}"
                        'https://api.atmo-aura.fr/api/v1/communes/69123/indices/atmo?date_debut_echeance={{yesterday}}'
                        '{% endwith %}{% endif %}&' + atmo_api_token,
                    },
                    {
                        'key': 'definitions',
                        'url': 'https://api.atmo-aura.fr/api/v1/indices/atmo/definitions?' + atmo_api_token,
                        'cache_duration': 86400,
                    },
                    {
                        'key': 'vigilances',
                        'url': 'https://api.atmo-aura.fr/api/v1/communes/69123/vigilances?date=now&'
                        + atmo_api_token,
                        'cache_duration': 300,
                    },
                    {
                        'key': 'nominatim',
                        'url': '{% if q_lat or lat %}{{passerelle_url}}base-adresse/nominatim/reverse'
                        '?lat={% firstof q_lat lat %}&lon={% firstof q_lon lon %}&format=json{% endif %}',
                        'cache_duration': 3600,
                    },
                    {
                        'key': 'places',
                        'url': '{% if user_nameid %}{{ passerelle_url }}jsondatastore/lieux-favoris/data/'
                        '?name_id={{user_nameid}}{% endif %}',
                    },
                    {
                        'key': 'place_geoloc',
                        'url': '{% if places %}{{ passerelle_url }}base-adresse/nominatim/search'
                        '?q={{ places.data.0.text }}&format=json{% endif %}',
                        'cache_duration': 3600,
                    },
                    {
                        'key': 'place2_geoloc',
                        'url': '{% if places|length > 1 %}{{ passerelle_url }}base-adresse/nominatim/search'
                        '?q={{ places.data.1.text }}&format=json{% endif %}',
                        'cache_duration': 3600,
                    },
                ],
                'actions': {
                    'geocode': {
                        'url': '{{ passerelle_url }}base-adresse/nominatim/search?q={{ q }}&format=json',
                        'method': 'get',
                        'response': 'raw',
                    }
                },
            },
            'pollen': {
                'url': 'https://download.data.grandlyon.com/ws/rdata/rnsa_reseau_aerobio.rnsabulletin/all.json',
                'cache_duration': 300,
                'force_async': False,
                'name': 'Info Pollen',
                'toodego:tile': True,
                'additional-data': [
                    {
                        'key': 'syntheses',
                        'url': 'https://download.data.grandlyon.com/ws/rdata/rnsa_reseau_aerobio.rnsabulletin/all.json'
                        '?field=type&value=Synthese',
                    }
                ],
            },
            'places': {
                'url': '{{passerelle_url}}jsondatastore/lieux-favoris/data/?name_id={{user_nameid}}',
                'name': 'Lieux favoris',
                'force_async': False,
                'cache_duration': 600,
                'additional-data': [{'key': 'userdata', 'url': '{{idp_url}}api/users/{{user_nameid}}'}],
                'actions': {
                    'create': {
                        'url': '{{passerelle_url}}jsondatastore/lieux-favoris/data/create?name_id={{user_nameid}}'
                    },
                    'update': {
                        'url': '{{passerelle_url}}jsondatastore/lieux-favoris/data/{{id}}/?name_id={{user_nameid}}'
                    },
                    'delete': {
                        'url': '{{passerelle_url}}jsondatastore/lieux-favoris/data/{{id}}/delete?name_id={{user_nameid}}'
                    },
                },
            },
            'silo-a-verre': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/silo-a-verre/'
                '?property:identifiant={{identifiant}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Silo à verre',
                'toodego:tile': True,
                'form': [{'varname': 'identifiant', 'type': 'string', 'label': 'Identifiant'}],
            },
            'tcl': {
                'url': '{{passerelle_url}}tcl/tcl/stop/{{id}}',
                'auto_refresh': 25,
                'cache_duration': 10,
                'force_async': False,
                'name': 'Arrêt TCL',
                'toodego:tile': True,
                'form': [{'varname': 'id', 'type': 'string', 'label': 'Identifiant'}],
            },
            'goto': {
                'url': '{% load l10n %}{% localize off %}{% if lat1 %}{{cityway_api_url}}api/journeyplanner/opt/PlanTrips/json'
                '?DepartureType=COORDINATES&DepartureLatitude={{lat1}}'
                '&DepartureLongitude={{lng1}}&ArrivalType=COORDINATES'
                '&ArrivalLatitude={{lat2}}&ArrivalLongitude={{lng2}}'
                '&Date={{date}}&DateType=DEPARTURE&TripModes={{tripmode}}'
                '&Algorithm=FASTEST&BikeSecurityLevel=None&BikeSpeed=12&WalkSpeed=4'
                '&MaxCarDistance=100&MaxBikeDistance=100&MaxWalkDistance=3000'
                '&AvoidDisruptions=1&StopToPlaceCar=1'
                '&SpecificOptions=GetOn;3|GetOff;4|TinyDistance;300|VLS;0|CARSHARING;0'
                "&UserId={% firstof cityway_api_userid 'toodego' %}&user_key={{cityway_api_userkey}}{% endif %}{% endlocalize %}",
                'cache_duration': 60,
                'force_async': False,
                'name': "M'y rendre",
                'loading-message': 'Calcul…',
                'varnames': ['lat1', 'lng1', 'lat2', 'lng2', 'date', 'to'],
                'form': [
                    {
                        'varname': 'tripmode',
                        'type': 'string',
                        'label': 'Mode de transport (WALK, BIKE, PT, CAR)',
                    }
                ],
            },
            'cut-profile': {
                'url': '{{idp_url}}api/users/{{user_nameid}}/',
                'name': 'Profil CUT',
                'auto_refresh': 60,
            },
            'profile-info': {'url': '{{idp_url}}api/users/{{user_nameid}}/', 'name': 'Infos de profil'},
            'suggestions': {
                'name': 'Suggestions',
                'url': '{{idp_url}}api/users/{{user_nameid}}',
                'force_async': True,
                'loading-message': 'Assemblage en cours, quelques secondes…',
                'additional-data': [
                    {
                        'key': 'places',
                        'url': '{{passerelle_url}}jsondatastore/lieux-favoris/data/?name_id={{user_nameid}}',
                    }
                ],
            },
            'alerte-qualite-de-l-air': {
                'url': '{{passerelle_url}}feeds/alertes-qualite-de-lair/json',
                'cache_duration': 600,
                'force_async': True,
                'name': "Alerte qualité de l'air",
            },
            'mairie-usager': {
                'name': "Mairie de l'usager",
                'url': '{{idp_url}}api/users/{{user_nameid}}/',
                'form': [{'varname': 'text', 'type': 'text', 'label': 'Texte'}],
            },
            'group-title': {
                'name': 'Intertitre',
                'url': '{{}}',
                'form': [{'varname': 'text', 'type': 'text', 'label': 'Texte'}],
            },
            'stats': {
                'name': 'Statistiques tuiles',
                'url': '{{portal_url}}api/dashboard/tile-stats/',
            },
            'parkingtr': {
                'url': 'https://%(data_username)s:%(data_password)s@download.data.grandlyon.com/wfs/rdata'
                '?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature'
                '&typename=pvo_patrimoine_voirie.pvoparkingtr'
                '&filter=<Filter><PropertyIsEqualTo><PropertyName>pkgid</PropertyName><Literal>[pkgid]</Literal></PropertyIsEqualTo></Filter>'
                % settings.DATA_CREDENTIALS,
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Parking',
                'toodego:tile': True,
                'form': [{'varname': 'pkgid', 'type': 'string', 'label': 'Identifiant'}],
            },
            'parc-velo': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/parc-velo/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Parc de stationnement vélos',
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'Identifiant'}],
            },
            'autopartage': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/autopartage/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Autopartage',
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'Identifiant'}],
            },
            'stationnement-pmr': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/stationnement-pmr/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Stationnement réglementé pour les PMR',
                'toodego:tile': 'pmr',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'Identifiant'}],
            },
            'toilette': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-general/query/toilette/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Toilette publique',
                'toodego:tile': True,
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'evenement-routier': {
                'url': 'https://%(data_username)s:%(data_password)s@download.data.grandlyon.com/wfs/rdata'
                '?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature&'
                'typename=pvo_patrimoine_voirie.pvoevenement'
                '&filter=<Filter><PropertyIsEqualTo><PropertyName>id</PropertyName><Literal>[id]</Literal></PropertyIsEqualTo></Filter>'
                % settings.DATA_CREDENTIALS,
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Evenement routier',
                'toodego:tile': True,
                'form': [{'varname': 'id', 'type': 'string', 'label': 'Identifiant'}],
            },
            'cartads-dossiers': {
                'url': '{{passerelle_url}}grandlyon-cartads-cs/{{slug}}/files?name_id={{user_nameid}}',
                'cache_duration': 0,
                'force_async': True,
                'name': 'Dossiers Cart@DS',
                'form': [{'varname': 'slug', 'type': 'string', 'label': 'Slug du connecteur'}],
                'actions': {
                    'unsubscribe': {
                        'url': '{{passerelle_url}}grandlyon-cartads-cs/{{slug}}/unsubscribe?name_id={{user_nameid}}',
                    },
                },
            },
            'urgence-sanitaire-soins': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/urgence-sanitaire-soins/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Urgence sanitaire Covid 19 - Accès aux soins',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/urgence-sanitaire.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'urgence-sanitaire-alimentation': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/urgence-sanitaire-alimentation/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Urgence sanitaire Covid 19 - Alimentation',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/urgence-sanitaire.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'urgence-sanitaire-hebergement': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/urgence-sanitaire-hebergement/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Urgence sanitaire Covid 19 - Hébergement',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/urgence-sanitaire.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'urgence-sanitaire-quotidien': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/urgence-sanitaire-quotidien/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Urgence sanitaire Covid 19 - Services au quotidien',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/urgence-sanitaire.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-animaux-jardin-bricolage': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-animaux-jardin-bricolage/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Animaux jardin bricolage',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-autres-activites': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-autres-activites/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Autres activités',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-deco-meubles': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-deco-meubles/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Déco meubles',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-fleuriste': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-fleuriste/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Fleuriste',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-jeux-jouets': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-jeux-jouets/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Jeux jouets',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-librairie': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-librairie/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Librairie',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-mode': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-mode/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Mode',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-restauration': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-restauration/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Restauration',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-soins-institut-beaute': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-soins-institut-beaute/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Soins institut beauté',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'commerce-sport-loisirs': {
                'url': '{{passerelle_url}}opengis/data-grandlyon-rdata/query/commerce-sport-loisirs/?property:gid={{gid}}',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Commerce - Sport loisirs',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/commerce.html',
                'form': [{'varname': 'gid', 'type': 'string', 'label': 'gid'}],
            },
            'rsj-a-traiter': {
                'url': '{{eservices_url}}api/forms/?limit={% firstof limit 100 %}{% if category_id %}'
                '&category_id={{ category_id }}{% endif %}&NameID={{user_nameid}}',
                'cache_duration': 120,
                'name': 'Demandes à traiter (RSJ)',
                'form': [
                    {'varname': 'limit', 'type': 'string', 'label': 'Nombre max de demandes'},
                    {'varname': 'category_id', 'type': 'string', 'label': 'Identifiant de la catégorie'},
                ],
            },
            'rsj-fiche': {
                'url': '{{eservices_url}}api/cards/jeunes-rsa/{{jeunes_rsa_id}}/',
                'name': 'Fiche RSJ',
                'cache_duration': 120,
            },
            'lieu-d-accueil': {
                'url': '{{eservices_url}}api/cards/ls-lieu-d-accueil/{% firstof raw_id ls_lieu_d_accueil_id %}/',
                'cache_duration': 86400,
                'force_async': False,
                'name': 'Lieu d’accueil',
                'toodego:page': True,
                'toodego:tile': True,
                'template-name': 'combo/json/lieu-d-accueil.html',
                'form': [{'label': 'Identifiant', 'type': 'string', 'varname': 'raw_id'}],
            },
            'caluire-famille': {
                'cache_duration': 30,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Infos famille',
                'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/family_info?NameID={{ user_nameid }}',
                'form': [{'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True}],
            },
            'caluire-responsables': {
                'cache_duration': 30,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Infos responsables',
                'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/family_info?NameID={{ user_nameid }}',
                'form': [{'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True}],
            },
            'caluire-liste-enfants': {
                'cache_duration': 30,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Liste des enfants (repeat)',
                'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/children_info?NameID={{ user_nameid }}',
                'make_global': 'list_children',
                'form': [{'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True}],
            },
            'caluire-enfant': {
                'cache_duration': 0,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Enfant (repeat)',
                'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/child_info?NameID={{ user_nameid }}'
                "&idpersonne={{ list_children.data|get:repeat_index|get:'id' }}",
                'repeat': '{{ list_children.data|length }}',
                'additional-data': [
                    {
                        'key': 'schooling',
                        'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/child_schooling_info?'
                        "NameID={{ user_nameid }}&idpersonne={{ list_children.data|get:repeat_index|get:'id' }}&schooling_date={% now 'Y-m-d' %}",
                    },
                    {
                        'key': 'activities',
                        'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/child_activities_info?'
                        "NameID={{ user_nameid }}&idpersonne={{ list_children.data|get:repeat_index|get:'id' }}&schooling_date={% now 'Y-m-d' %}",
                    },
                ],
                'form': [
                    {'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True},
                ],
            },
            'caluire-enfant-detail': {
                'cache_duration': 30,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Enfant',
                'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/child_info?NameID={{ user_nameid }}&idpersonne={{ child_id }}',
                'form': [
                    {'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True},
                ],
            },
            'caluire-enfant-prochaines-inscriptions': {
                'cache_duration': 0,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Prochaines inscriptions',
                'url': "{% now 'n' as current_month %}{% if current_month in '678' %}"
                '{{ passerelle_url }}caluire-axel/{{ slug }}/child_activities_info?NameID={{ user_nameid }}&idpersonne={{ child_id }}'
                "&schooling_date={% now 'Y' as current_year %}{{ current_year|add:'1' }}-{% now 'm-d' %}{% endif %}",
                'form': [
                    {'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True},
                ],
            },
            'caluire-enfant-agenda': {
                'cache_duration': 0,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Agenda',
                'url': '{{ passerelle_url }}caluire-axel/{{ slug }}/get_agenda_full?NameID={{ user_nameid }}&idpersonne={{ child_id }}'
                "{% now 'Y' as current_year %}{% now 'n' as current_month %}"
                "&start_date={% if current_month in '87654321' %}{{ current_year|add:'-1' }}{% else %}{{ current_year }}{% endif %}-09-01"
                "&end_date={% if current_month in '87654321' %}{{ current_year }}{% else %}{{ current_year|add:'1' }}{% endif %}-08-31",
                'form': [
                    {'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True},
                ],
            },
            'caluire-enfant-agenda-next-year': {
                'cache_duration': 0,
                'force_async': True,
                'name': 'Caluire-et-Cuire - Teamnet Axel: Agenda prochaine année scolaire',
                'url': "{% now 'Y' as current_year %}{% now 'n' as current_month %}{% if current_month in '678' %}"
                '{{ passerelle_url }}caluire-axel/{{ slug }}/get_agenda_full?NameID={{ user_nameid }}&idpersonne={{ child_id }}'
                "&start_date={% if current_month in '87654321' %}{{ current_year }}{% else %}{{ current_year|add:'1' }}{% endif %}-09-01"
                "&end_date={% if current_month in '87654321' %}{{ current_year|add:'1' }}{% else %}{{ current_year|add:'2' }}{% endif %}-08-31{% endif %}",
                'form': [
                    {'label': 'Identifiant du connecteur', 'varname': 'slug', 'required': True},
                ],
            },
        }

        PRODUCER_LABELS = {
            'atmo': 'ATMO Auvergne Rhône-Alpes',
            'rnsa': 'RNSA',
            'onlymoov': 'ONLY MOOV',
            'tcl': 'TCL',
            'toodego': 'Toodego',
            'velov': "Velo'v",
            'vivrou': 'Vivro',
            'grandlyon': 'Grand Lyon',
            'bluely': 'Bluely',
            'citiz-lpa': 'Citiz LPA',
            'sncf': 'SNCF',
            'effia': 'EFFIA',
            'indigo': 'Indigo',
            'lpa': 'LPA',
            'sas-parc-opera': 'SAS Parc Opera',
            'q-park': 'Q-Park',
            'unibail': 'Unibail',
            'sajr': 'sajr',
            'aeroport-saint-exupe': 'Aéroport Saint Exupe',
        }

        COLLECTIVITY_LABELS = {
            'albigny-sur-saone': 'Albigny-sur-Saône',
            'bron': 'Bron',
            'cailloux-sur-fontaines': 'Cailloux-sur-Fontaines',
            'caluire-et-cuire': 'Caluire-et-Cuire',
            'champagne-au-mont-d-or': "Champagne-au-Mont-d'Or",
            'charbonnieres-les-bains': 'Charbonnières-les-Bains',
            'charly': 'Charly',
            'chassie': 'Chassie',
            'collonges-au-mont-d-or': "Collonges-au-Mont-d'Or",
            'corbas': 'Corbas',
            'couzon-au-mont-d-or': "Couzon-au-Mont-d'Or",
            'craponne': 'Craponne',
            'curis-au-mont-d-or': "Curis-au-Mont-d'Or",
            'dardilly': 'Dardilly',
            'decines-charpie': 'Décines-Charpie',
            'ecully': 'Écully',
            'feyzin': 'Feyzin',
            'fleurieu-sur-saone': 'Fleurieu-sur-Saône',
            'fontaines-saint-martin': 'Fontaines-Saint-Martin',
            'fontaines-sur-saone': 'Fontaines-sur-Saône',
            'francheville': 'Francheville',
            'genay': 'Genay',
            'givors': 'Givors',
            'grigny': 'Grigny',
            'irigny': 'Irigny',
            'jonage': 'Jonage',
            'la-mulatiere': 'La Mulatière',
            'la-tour-de-salvagny': 'La Tour de Salvagny',
            'limonest': 'Limonest',
            'lissie': 'Lissie',
            'lyon': 'Lyon',
            'marcy-l-etoile': "Marcy-l'Étoile",
            'meyzie': 'Meyzie',
            'mions': 'Mions',
            'montanay': 'Montanay',
            'neuville-sur-saone': 'Neuville-sur-Saône',
            'oullins': 'Oullins',
            'oullinspierrebenite': 'Oullins-Pierre-Bénite',
            'pierre-benite': 'Pierre-Bénite',
            'poleymieux-au-mont-d-or': "Poleymieux-au-Mont-d'Or",
            'quincieux': 'Quincieux',
            'rillieux-la-pape': 'Rillieux-la-Pape',
            'rochetaillee-sur-saone': 'Rochetaillée-sur-Saône',
            'saint-cyr-au-mont-d-or': "Saint-Cyr-au-Mont-d'Or",
            'saint-didier-au-mont-d-or': "Saint-Didier-au-Mont-d'Or",
            'saint-fons': 'Saint-Fons',
            'saint-genis-laval': 'Saint-Genis-Laval',
            'saint-genis-les-ollieres': 'Saint-Genis-les-Ollières',
            'saint-germain-au-mont-d-or': "Saint-Germain-au-Mont-d'Or",
            'saint-priest': 'Saint-Priest',
            'sainte-foy-les-lyon': 'Sainte-Foy-lès-Lyon',
            'sathonaycamp': 'Sathonay-Camp',
            'sathonay-village': 'Sathonay-Village',
            'solaize': 'Solaize',
            'st-romain-au-mont-d-or': "Saint-Romain-au-Mont-d'Or",
            'tassin-la-demi-lune': 'Tassin-la-Demi-Lune',
            'vaulx-en-velin': 'Vaulx-en-Velin',
            'venissieux': 'Venissieux',
            'vernaison': 'Vernaison',
            'villeurbanne': 'Villeurbanne',
        }

        PRODUCER_LABELS.update(COLLECTIVITY_LABELS)

        COMBO_ASSET_SLOTS = self.settings_json.get('COMBO_ASSET_SLOTS.update') or {}
        for producer, producer_label in PRODUCER_LABELS.items():
            COMBO_ASSET_SLOTS['logo:%s' % producer] = {'label': 'Logo %s' % producer_label}

        # add lyon as used by cart@ds
        for arrondissement in ['1er'] + ['%seme' % x for x in range(2, 10)]:
            COMBO_ASSET_SLOTS['logo:lyon-%s' % arrondissement] = {
                'label': 'Logo Lyon %s' % arrondissement,
            }

        for cell_key, cell_dict in JSON_CELL_TYPES.items():
            tile_label = cell_dict.get('name')
            if cell_dict.get('toodego:tile'):
                tile_key = (
                    cell_dict.get('toodego:tile')
                    if isinstance(cell_dict.get('toodego:tile'), str)
                    else cell_key
                )
                COMBO_ASSET_SLOTS['picture:%s' % tile_key] = {'label': 'Photographie %s' % tile_label}
            if cell_dict.get('toodego:page'):
                page_key = (
                    cell_dict.get('toodego:page')
                    if isinstance(cell_dict.get('toodego:page'), str)
                    else cell_key
                )
                COMBO_ASSET_SLOTS['picture:banner:%s' % page_key] = {
                    'label': 'Photographie/bannière %s' % tile_label
                }

        # noqa pylint: disable=possibly-unused-variable
        SESSION_EXPIRE_AT_BROWSER_CLOSE = False

        self.settings_json.update({k: v for k, v in locals().items() if k[0].isupper()})

    def import_extra_settings(self):
        with open(self.extra_settings_filepath) as fd:
            settings_json = json.load(fd)
        for k, v in settings_json.items():
            if isinstance(self.settings_json.get(k), dict):
                self.settings_json[k].update(v)
            else:
                self.settings_json[k] = v

    def set_no_logs(self):
        # change json cells to never log
        for cell in self.settings_json['JSON_CELL_TYPES'].values():
            cell['log_errors'] = False
            for add in cell.get('additional-data') or []:
                add['log_errors'] = False
