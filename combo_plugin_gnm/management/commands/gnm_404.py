# combo-plugin-gnm - Combo GNM plugin
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    def handle(self, *args, **options):
        dirname = os.path.join(connection.tenant.get_directory(), 'html')
        if not os.path.exists(dirname):
            return
        filename = os.path.join(dirname, '404.html')
        url = settings.SITE_BASE_URL + '/404/'
        subprocess.call(['wget', '--quiet', '-O', filename, '--convert-links', url])
